package ru.t1.gorodtsova.tm.api.service;

import ru.t1.gorodtsova.tm.enumerated.Status;
import ru.t1.gorodtsova.tm.model.Task;

import java.util.List;

public interface ITaskService extends IUserOwnerService<Task> {

    Task create(String userId, String name);

    Task create(String userId, String name, String description);

    List<Task> findAllByProjectId(String userId, String projectId);

    Task updateByIndex(String userId, Integer index, String name, String description);

    Task updateById(String userId, String id, String name, String description);

    Task changeTaskStatusByIndex(String userId, Integer index, Status status);

    Task changeTaskStatusById(String userId, String id, Status status);

}
