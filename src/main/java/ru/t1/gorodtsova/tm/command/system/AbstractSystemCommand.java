package ru.t1.gorodtsova.tm.command.system;

import ru.t1.gorodtsova.tm.api.service.ICommandService;
import ru.t1.gorodtsova.tm.command.AbstractCommand;
import ru.t1.gorodtsova.tm.enumerated.Role;

public abstract class AbstractSystemCommand extends AbstractCommand {

    protected ICommandService getCommandService() {
        return serviceLocator.getCommandService();
    }

    @Override
    public Role[] getRoles() {
        return null;
    }

}
