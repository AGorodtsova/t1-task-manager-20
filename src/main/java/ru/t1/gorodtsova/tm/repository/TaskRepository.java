package ru.t1.gorodtsova.tm.repository;

import ru.t1.gorodtsova.tm.api.repository.ITaskRepository;
import ru.t1.gorodtsova.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public final class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    @Override
    public Task create(final String userId, String name) {
        final Task task = new Task();
        task.setName(name);
        return add(userId, task);
    }

    @Override
    public Task create(final String userId, String name, String description) {
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        return add(userId, task);
    }

    @Override
    public List<Task> findAllByProjectId(final String userId, final String projectId) {
        final List<Task> result = new ArrayList<>();
        for (final Task task : records) {
            if (task.getProjectId() == null) continue;
            if (!userId.equals(task.getUserId())) continue;
            if (task.getProjectId().equals(projectId)) result.add(task);
        }
        return result;
    }

}